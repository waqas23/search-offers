<?php

namespace App\Service\SearchOffers;

interface SearchOffersInterface
{
    public function searchByPrice(float $from, float $to): int;

    public function searchByVendor(int $vendorId): int;
}