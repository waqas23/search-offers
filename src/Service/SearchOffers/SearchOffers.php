<?php

namespace App\Service\SearchOffers;

use App\Model\Offer;
use App\Service\OfferReader\ReaderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SearchOffers implements SearchOffersInterface
{
    private $filePath;

    public function __construct(
        private LoggerInterface $logger,
        private ReaderInterface $reader,
        private ParameterBagInterface $param,
    )
    {
        $this->filePath = $this->param->get('file_path');
    }

    public function searchByPrice(float $from, float $to): int
    {
        try {
            $offers = $this->reader->read($this->filePath);
            $totalQuantity = 0;

            /** @var Offer $offer */
            foreach ($offers as $offer) {
                if ($offer->getPrice() > $from && $offer->getPrice() < $to) {
                    $totalQuantity = $totalQuantity + $offer->getQuantity();
                }
            }

            return $totalQuantity;
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());

            return -1;
        }

    }

    public function searchByVendor(int $vendorId): int
    {
        try {
            $offers = $this->reader->read($this->filePath);
            $totalQuantity = 0;

            /** @var Offer $offer */
            foreach ($offers as $offer) {
                if ($offer->getVendorId() == $vendorId) {
                    $totalQuantity = $totalQuantity + $offer->getQuantity();
                }
            }

            return $totalQuantity;
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());

            return -1;
        }
    }

}