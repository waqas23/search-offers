<?php

namespace App\Service\OfferReader;

use App\Service\OfferCollection\OfferCollectionInterface;

interface ReaderInterface
{

    /**
     * Read in incoming data and parse to objects
     */
    public function read(string $input): OfferCollectionInterface;

}