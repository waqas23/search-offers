<?php

namespace App\Service\OfferReader;

use App\Model\Offer;
use App\Service\OfferCollection\OfferCollection;
use App\Service\OfferCollection\OfferCollectionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;

class FileStreamReader implements ReaderInterface
{
    public function __construct(
        private Filesystem      $filesystem,
        private LoggerInterface $logger
    )
    {
    }


    public function read(string $input): OfferCollectionInterface
    {
        $collection = new OfferCollection();

        $data = $this->getFileContent($input);

        if ($data == null) {
            $this->logger->error(sprintf('File not exists'));
        }
        $offer_list = json_decode($data, true);

        foreach ($offer_list as $offer) {
            $offer = Offer::createFromArray([
                Offer::OFFER_ID      => $offer[Offer::OFFER_ID],
                Offer::PRODUCT_TITLE => $offer[Offer::PRODUCT_TITLE],
                Offer::VENDOR_ID     => $offer[Offer::VENDOR_ID],
                Offer::PRICE         => $offer[Offer::PRICE],
                Offer::QUANTUTY      => $offer[Offer::QUANTUTY],
            ]);
            $collection->add($offer);
        }

        return $collection;
    }


    private function getFileContent(string $filePath): ?string
    {
        if (!$this->filesystem->exists($filePath)) {
            return null;
        }

        return file_get_contents($filePath);
    }

}