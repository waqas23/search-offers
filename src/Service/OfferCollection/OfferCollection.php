<?php

namespace App\Service\OfferCollection;

use App\Model\OfferInterface;
use Iterator;

class OfferCollection extends ListRef implements OfferCollectionInterface
{
    public function get(int $index): ?OfferInterface
    {
        if (array_key_exists($index, $this->list)) {
            return $this->list[$index];
        }

        return null;
    }

    public function getIterator(): Iterator
    {
        return $this;
    }

}