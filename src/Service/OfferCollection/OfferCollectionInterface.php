<?php

namespace App\Service\OfferCollection;

use App\Model\OfferInterface;
use Iterator;

interface OfferCollectionInterface
{
    public function get(int $index): ?OfferInterface;

    public function getIterator(): Iterator;

}