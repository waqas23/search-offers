<?php

namespace App\Model;

interface OfferInterface
{
    /**
     * @param array $data
     *
     * @return static
     */
    public static function createFromArray(array $data): self;
}