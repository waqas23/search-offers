<?php

namespace App\Model;

use App\Service\OfferCollection\ListRef;

class Offer extends ListRef implements OfferInterface
{
    const OFFER_ID = 'offerId';
    const PRODUCT_TITLE = 'productTitle';
    const VENDOR_ID = 'vendorId';
    const PRICE = 'price';
    const QUANTUTY = 'quantity';

    public $offerId;
    public $productTitle;
    public $vendorId;
    public $price;
    public $quantity;

    /**
     * @return mixed
     */
    public function getOfferId(): int
    {
        return $this->offerId;
    }

    /**
     * @param mixed $offerId
     */
    public function setOfferId(int $offerId): void
    {
        $this->offerId = $offerId;
    }

    /**
     * @return mixed
     */
    public function getProductTitle(): string
    {
        return $this->productTitle;
    }

    /**
     * @param mixed $productTitle
     */
    public function setProductTitle(string $productTitle): void
    {
        $this->productTitle = $productTitle;
    }

    /**
     * @return mixed
     */
    public function getVendorId(): int
    {
        return $this->vendorId;
    }

    /**
     * @param mixed $vendorId
     */
    public function setVendorId(int $vendorId): void
    {
        $this->vendorId = $vendorId;
    }

    /**
     * @return mixed
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public static function createFromArray(array $data): self
    {
        $obj = new static;
        $obj->setOfferId($data[self::OFFER_ID] ?? null);
        $obj->setProductTitle($data[self::PRODUCT_TITLE] ?? null);
        $obj->setVendorId($data[self::VENDOR_ID] ?? null);
        $obj->setPrice($data[self::PRICE] ?? null);
        $obj->setQuantity($data[self::QUANTUTY] ?? null);

        return $obj;
    }

}