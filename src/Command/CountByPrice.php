<?php

namespace App\Command;

use App\Service\SearchOffers\SearchOffersInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CountByPrice extends Command
{
    const TO = 'to';
    const FROM = 'from';
    protected static $defaultName = 'offer:count:price';

    public function __construct(
        private LoggerInterface $logger,
        private SearchOffersInterface $searchOffers
    )
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('Product count by price')
            ->setDescription('This command count product by price')
            ->addArgument(self::FROM, InputArgument::REQUIRED)
            ->addArgument(self::TO, InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $priceFrom = floatval($input->getArgument(self::FROM));
        $priceTo = floatval($input->getArgument(self::TO));

        if ($priceFrom > $priceTo) {
            $this->logger->error('Invalid price range');
            $output->writeln(sprintf('<error>Invalid price range</error>'));

            return Command::FAILURE;
        }

        $quantity = $this->searchOffers->searchByPrice($priceFrom, $priceTo);

        if ($quantity == -1) {
            $output->writeln(sprintf('<error>Something went wrong</error>'));

            return Command::FAILURE;
        } else {
            $output->writeln(sprintf('<info>Total Quantity available %d</info>', $quantity));

            return Command::SUCCESS;
        }
    }

}