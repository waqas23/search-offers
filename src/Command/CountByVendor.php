<?php

namespace App\Command;

use App\Service\SearchOffers\SearchOffersInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CountByVendor extends Command
{
    const VENDOR_ID = 'vendorId';
    protected static $defaultName = 'offer:count:vendor';

    public function __construct(
        private SearchOffersInterface $searchOffers
    )
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('Product count by vendor')
            ->setDescription('This command count product by vendor')
            ->addArgument(self::VENDOR_ID, InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $vendorId = (int)$input->getArgument(self::VENDOR_ID);

        $quantity = $this->searchOffers->searchByVendor($vendorId);

        if ($quantity == -1) {
            $output->writeln(sprintf('<error>Something went wrong</error>'));

            return Command::FAILURE;
        } else {
            $output->writeln(sprintf('<info>Total Quantity available %d</info>', $quantity));

            return Command::SUCCESS;
        }
    }

}