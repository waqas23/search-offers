Setup Guide
--------------
Clone project
```
git clone https://waqas23@bitbucket.org/waqas23/search-offers.git
```

Please run ```composer install``` to update vendors

Please run following command on the project root to start project

```php bin/console server:start```

This project support following command

* Search by price range which accepts two parameters, from and to 
```
bin/console offer:count:price 10 100
``` 
* Search by vendor which accepts one parameter, vendor Id
```
bin/console offer:count:vendor 35
```


Please run following command to run test cases

```./vendor/bin/phpunit```